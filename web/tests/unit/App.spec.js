import { shallowMount } from '@vue/test-utils'
import App from '@/App.vue'
import TodoList from '@/components/TodoList.vue'

const wrapper = shallowMount(App)

describe('App.vue', () => {
  it('should render', () => {
    expect(wrapper.find('h1').text()).toBe('todo list')
    expect(wrapper.find('.qa-todo-list').is(TodoList)).toBe(true)
  })
})
