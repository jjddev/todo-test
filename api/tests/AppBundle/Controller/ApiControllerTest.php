<?php

namespace AppBundle\Tests\Controller;

use AppBundle\Entity\Task;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\NotNull;

class ApiControllerTest extends WebTestCase
{
    /**
     * Creates the client and router to be used on the tests
     */
    protected function setUp()
    {
        $this->client = static::createClient();
        $this->router = $this->client->getContainer()->get('router');
    }

    /**
     * Creates a task and persists it on the database
     * 
     * @param string $title
     * @param boolean $completed
     * @return AppBundle\Entity\Task
     */
    protected function createTask($title = "Task title", $completed = false)
    {
        $task = new Task();
        $task->setTitle($title);
        $task->setCompleted($completed);

        $em = $this->client->getContainer()->get('doctrine')->getManager();
        $em->persist($task);
        $em->flush();

        return $task;        
    }

    /**
     * Tests listing all tasks (GET /api/v1/tasks)
     */
    public function testListTasks()
    {
        $url = $this->router->generate('task_list');
        $this->client->request('GET', $url);
        $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $decodedData = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertContains("result", array_keys($decodedData));
    }

    /**
     * Tests creating a new task (POST /api/v1/tasks)
     */
    public function testCreateTaskSuccess()
    {
        $title = "Test task title #" . random_int(999, 9999);
        $completed = (bool)random_int(0, 1); 
        $url = $this->router->generate('task_create');
        $this->client->request('POST', $url,
            [],
            [],
            ['HTTP_Content-Type' => 'application/json'],
            json_encode(['title' => $title, 'completed' => $completed])
        );
        $this->assertEquals(Response::HTTP_CREATED, $this->client->getResponse()->getStatusCode());
        $decodedData = json_decode($this->client->getResponse()->getContent());
        $this->assertEquals($title, $decodedData->title);
        $this->assertEquals($completed, $decodedData->completed);
    }

    /**
     * Tests creating a new task with missing title (POST /api/v1/tasks)
     */
    public function testCreateTaskMissingTitle()
    {
        $url = $this->router->generate('task_create');
        $this->client->request('POST', $url,
            [],
            [],
            ['HTTP_Content-Type' => 'application/json'],
            json_encode(['completed' => false])
        );
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $this->client->getResponse()->getStatusCode());
        $this->assertContains((new NotNull())->message, $this->client->getResponse()->getContent());
    }

    /**
     * Tests updating non-existing task (PUT /api/v1/tasks/{id})
     */
    public function testUpdateNonExistingTask()
    {
        $url = $this->router->generate('task_update', ['taskId' => random_int(9999, 99999)]);
        $this->client->request('PUT', $url,
            [],
            [],
            ['HTTP_Content-Type' => 'application/json']
        );
        $this->assertEquals(Response::HTTP_NOT_FOUND, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Tests updating existing task (PUT /api/v1/tasks/{id})
     */
    public function testUpdateTaskSuccess()
    {
        $task = $this->createTask();
        $new_title = "New task title";
        $url = $this->router->generate('task_update', ['taskId' => $task->getId()]);
        $this->client->request('PUT', $url,
            [],
            [],
            ['HTTP_Content-Type' => 'application/json'],
            json_encode(['title' => $new_title, 'completed' => false])
        );
        $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $decodedData = json_decode($this->client->getResponse()->getContent());
        $this->assertEquals($new_title, $decodedData->title);
    }

    /**
     * Tests updating existing task with missing title (PUT /api/v1/tasks/{id})
     */
    public function testUpdateTaskMissingTitle()
    {
        $task = $this->createTask();
        $url = $this->router->generate('task_update', ['taskId' => $task->getId()]);
        $this->client->request('PUT', $url,
            [],
            [],
            ['HTTP_Content-Type' => 'application/json'],
            json_encode(['completed' => false])
        );
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $this->client->getResponse()->getStatusCode());
        $decodedData = json_decode($this->client->getResponse()->getContent());
        $this->assertContains((new NotNull())->message, $decodedData->errors->title);
    }

    /**
     * Tests retrieving existing task (GET /api/v1/tasks/{id})
     */
    public function testRetrieveTaskSuccess()
    {
        $task = $this->createTask();
        $em = $this->client->getContainer()->get('doctrine')->getManager();
        $em->persist($task);
        $em->flush();

        $url = $this->router->generate('task_retrieve', ['taskId' => $task->getId()]);
        $this->client->request('GET', $url,
            [],
            [],
            ['HTTP_Content-Type' => 'application/json']
        );

        $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $decodedData = json_decode($this->client->getResponse()->getContent());
        $this->assertEquals($task->getTitle(), $decodedData->title);
        $this->assertEquals($task->getCompleted(), $decodedData->completed);
    }

    /**
     * Tests retrieving non-existing task (GET /api/v1/tasks/{id})
     */
    public function testRetrieveTaskNotFound()
    {
        $url = $this->router->generate('task_update', ['taskId' => random_int(9999, 99999)]);
        $this->client->request('GET', $url,
            [],
            [],
            ['HTTP_Content-Type' => 'application/json']
        );
        $this->assertEquals(Response::HTTP_NOT_FOUND, $this->client->getResponse()->getStatusCode());
    }
}
